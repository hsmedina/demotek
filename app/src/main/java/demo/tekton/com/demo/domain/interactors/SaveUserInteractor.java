package demo.tekton.com.demo.domain.interactors;



import javax.inject.Inject;

import demo.tekton.com.demo.domain.repository.UserRepository;
import demo.tekton.com.demo.domain.util.Constants;
import demo.tekton.com.demo.domain.util.Params;
import io.reactivex.Observable;



public class SaveUserInteractor extends  UseCase<Object, Params> {

  private final UserRepository userRepository;

  @Inject
  public SaveUserInteractor(UserRepository userRepository) {
    this.userRepository = userRepository;
  }


  @Override
  Observable<Object> buildUseCaseObservable(Params params) {
    return this.userRepository.saveUserInDB(params.getString(Constants.PARAMS_USER_ID, null),
            params.getString(Constants.PARAM_USER, null));
  }


}