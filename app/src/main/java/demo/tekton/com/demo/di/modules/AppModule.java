package demo.tekton.com.demo.di.modules;

import android.app.Application;
import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hsmedina on 3/19/2018.
 */


@Module
public class AppModule {

    Application application;

    public AppModule(Application application){
        this.application=application;
    }

    @Provides
    public Context provideContextApplication(){
        return application.getApplicationContext();
    }

}
