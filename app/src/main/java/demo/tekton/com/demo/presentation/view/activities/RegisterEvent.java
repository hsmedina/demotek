package demo.tekton.com.demo.presentation.view.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import demo.tekton.com.demo.R;
import demo.tekton.com.demo.di.components.DaggerEventComponent;
import demo.tekton.com.demo.di.components.EventComponent;
import demo.tekton.com.demo.di.modules.EventModule;
import demo.tekton.com.demo.presentation.presenter.RegisterEventPresenterImpl;
import demo.tekton.com.demo.presentation.view.contracts.RegisterEventContract;
import demo.tekton.com.demo.presentation.view.util.Util;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class RegisterEvent extends AppCompatActivity implements RegisterEventContract.view,  TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener  {


    @BindView(R.id.new_event) EditText name_event;
    @BindView(R.id.date_event) TextView date_event;
    @BindView(R.id.time_event) TextView time_event;
    @BindView(R.id.address_event) TextView address_event;
    @BindView(R.id.loader) ProgressBar loader;
    @BindView(R.id.btnreg) FloatingActionButton fab;
    @BindView(R.id.toolbar) Toolbar toolbar;



    private Calendar calendar = Calendar.getInstance();
    private int startYear;
    private int startMonth;
    private int startDay;
    private int hourOfDay;
    private int minuteOfDay;

    EventComponent component;

    @Inject RegisterEventPresenterImpl presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_events);
        ButterKnife.bind(this);
        initDagger();
        initToolBar();
        initDatePicker();
        initTimePicker();
        presenter.setView(this);
        address_event.setText("Lima - Peru");

    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loader.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMessage() {

    }

    @Override
    public void showErrorMessage(String errorMessage) {

    }

    @OnClick(R.id.btnreg) void OnclickNewEvent(){

        if(Util.validationEmptyInput(name_event, getString(R.string.msg_event_validate))){
            createEvent(name_event.getText().toString(),
                    date_event.getText().toString(),
                    time_event.getText().toString(),
                    address_event.getText().toString());
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case android.R.id.home:
                resetData();
                startActivity(new Intent(this,HomeActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void createEvent(String name, String date, String time, String location) {
        presenter.createEvent(name,date,time,location);
    }

    @Override
    public void createSuccess() {
        Util.showMessager(address_event,getString(R.string.registerSuccess));
        resetData();
    }

    private void resetData(){
        initDatePicker();
        initTimePicker();
        name_event.setText("");
    }

    @Override
    public void createFalied() {
        Util.showMessager(address_event,getString(R.string.registerSuccess));
    }

    @OnClick(R.id.date_event_row) void eventDate() {
        DatePickerDialog dialog = new DatePickerDialog(this, this, startYear, startMonth, startDay);
        dialog.show();
    }

    @OnClick(R.id.time_event_row) void timeEvent() {
        TimePickerDialog dialog = new TimePickerDialog(this, this, hourOfDay, minuteOfDay, false);
        dialog.show();
    }


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);

        if (!Util.isBeforeToday(calendar)) {
            this.hourOfDay = hourOfDay;
            this.minuteOfDay = minute;

            final String displayedTimeEvent = Util.getDisplayTime(hourOfDay, minuteOfDay);
            time_event.setText(displayedTimeEvent);

        } else {
            Util.showMessager(date_event,getString(R.string.error_date_event));
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        if (!Util.isBeforeToday(calendar)) {
            startYear = year;
            startMonth = month;
            startDay = dayOfMonth;

            calendar.set(startYear, startMonth, startDay);

            final String displayedDayEvent = Util.getDisplayDate(this, calendar);
            date_event.setText(displayedDayEvent);
        } else {
            Util.showMessager(date_event,getString(R.string.error_date_event));
        }

    }


    public void initDatePicker() {
        startYear = calendar.get(Calendar.YEAR);
        startMonth = calendar.get(Calendar.MONTH);
        startDay = calendar.get(Calendar.DAY_OF_MONTH);

        final String displayedDayEvent = Util.getDisplayDate(this, calendar);
        date_event.setText(displayedDayEvent);
    }

    public void initTimePicker() {
        hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        minuteOfDay = calendar.get(Calendar.MINUTE);


        final String displayedTimeEvent = Util.getDisplayTime(hourOfDay, minuteOfDay);
        time_event.setText(displayedTimeEvent);
    }


    private void initDagger(){
        if (component == null) {
            component = DaggerEventComponent.builder()
                    .eventModule(new EventModule())
                    .build();
            component.inject(this);
        }
    }

    private void initToolBar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getString(R.string.event));

    }
}
