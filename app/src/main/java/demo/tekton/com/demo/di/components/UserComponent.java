package demo.tekton.com.demo.di.components;

import javax.inject.Singleton;

import dagger.Component;
import demo.tekton.com.demo.di.modules.UserModule;
import demo.tekton.com.demo.presentation.view.activities.LoginActivity;
import demo.tekton.com.demo.presentation.view.activities.LogoutActivity;
import demo.tekton.com.demo.presentation.view.activities.RegisterActivity;
import demo.tekton.com.demo.presentation.view.activities.SplashActivity;

/**
 * Created by hsmedina on 3/18/2018.
 */

@Singleton
@Component(modules = UserModule.class)
public interface UserComponent {
    void inject(RegisterActivity view);
    void inject(LoginActivity view);
    void inject(LogoutActivity view);
    void inject(SplashActivity view);
}
