package demo.tekton.com.demo.presentation.view.contracts;

import demo.tekton.com.demo.presentation.presenter.BasePesenter;
import demo.tekton.com.demo.presentation.view.base.BaseView;

/**
 * Created by hsmedina on 3/19/2018.
 */

public interface SplashContract {

    interface view extends BaseView {
        void goLogin();
        void goHome();
    }

    interface presenter extends BasePesenter<SplashContract.view> {
        void checkSession();
        void goLogin();
        void goHome();
    }
}
