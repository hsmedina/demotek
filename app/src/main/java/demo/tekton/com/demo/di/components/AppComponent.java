package demo.tekton.com.demo.di.components;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Singleton;
import dagger.Component;
import demo.tekton.com.demo.di.modules.AppModule;

/**
 * Created by hsmedina on 3/19/2018.
 */


@Component(modules = {AppModule.class})
public interface AppComponent {
    Context context();
}
