package demo.tekton.com.demo.presentation.view.util;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import demo.tekton.com.demo.R;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class Util {



    public static void setFontEditTextStyle(Context context, TextView view){
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "Montserrat-Light.otf");
        view.setTypeface(tf);
    }

    public static Drawable getDrawableImage(Context context, int img){
        if(greaterLollipop())
            return context.getDrawable(img);
        else
            return context.getResources().getDrawable(img);
    }



    public static boolean greaterLollipop(){
        if (Build.VERSION.SDK_INT > 21) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean validationEmpty(Context context, EditText user, EditText pass){

            boolean valid = true;

            String email = user.getText().toString();
            String password = pass.getText().toString();

            if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                user.setError(context.getString(R.string.msg_email_validate));
                valid = false;
            } else {
                user.setError(null);
            }

            if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
                pass.setError(context.getString(R.string.msg_pass_validate));
                valid = false;
            } else {
                pass.setError(null);
            }

            return valid;

    }

    public static boolean validationPassword(Context context, EditText pass, EditText confpass){

        boolean valid = true;


        if (!pass.getText().toString().trim().equals(confpass.getText().toString().trim())) {
            confpass.setError(context.getString(R.string.msg_confpass_validate));
            valid = false;
        } else {
            confpass.setError(null);
        }

        return valid;

    }


    public static boolean validationEmptyInput(EditText input, String message){

        boolean valid = true;

        String txt = input.getText().toString();

        if (txt.isEmpty()) {
            input.setError(message);
            valid = false;
        } else {
            input.setError(null);
        }

        return valid;

    }


    public  static void showMessager(View view, String message){
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);

        snackbar.show();
    }


    public static String getDisplayDate(@NonNull Context context,
                                        @NonNull Calendar calendar) {
        if (Util.isToday(calendar)) {
            return context.getResources().getString(R.string.today);
        }
        else if (Util.isTomorrow(calendar)) {
            return context.getResources().getString(R.string.tomorrow);
        }
        else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, d MMM");
            return dateFormat.format(calendar.getTime());
        }
    }

    public static boolean isBeforeToday(Calendar calendar) {
        return calendar.before(Calendar.getInstance());
    }


    public static boolean isToday(Calendar calendar) {
        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.DAY_OF_MONTH)==calendar.get(Calendar.DAY_OF_MONTH)) {
            if (now.get(Calendar.MONTH)==calendar.get(Calendar.MONTH)) {
                if (now.get(Calendar.YEAR)==calendar.get(Calendar.YEAR)) {
                    return true;
                }
            }
        }

        return false;
    }


    public static boolean isTomorrow(Calendar calendar) {
        Calendar now = Calendar.getInstance();
        if ((now.get(Calendar.DAY_OF_MONTH)+1)==calendar.get(Calendar.DAY_OF_MONTH)) {
            if (now.get(Calendar.MONTH)==calendar.get(Calendar.MONTH)) {
                if (now.get(Calendar.YEAR)==calendar.get(Calendar.YEAR)) {
                    return true;
                }
            }
        }

        return false;
    }


    public static String getDisplayTime(int hourOfDay,
                                        int minute) {
        DateFormat dateFormat = new SimpleDateFormat("h:mm a");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);

        return dateFormat.format(calendar.getTime());
    }


   /* public static Bundle detailsParameters(String id , String name, String date, String time, String location){
        Bundle args = new Bundle();
        args.putSerializable("1", id);
        args.putSerializable("2", name);
        args.putSerializable(key, date);
        args.putSerializable(key, time);
        args.putSerializable(key, location);
        return args;
    } */

}
