package demo.tekton.com.demo.presentation.view.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import demo.tekton.com.demo.R;
import demo.tekton.com.demo.presentation.view.fragments.EventsDetailFragment;
import demo.tekton.com.demo.presentation.view.fragments.EventsFragment;
import demo.tekton.com.demo.presentation.view.fragments.MainFragment;
import demo.tekton.com.demo.presentation.view.listeners.DetailsListener;
import demo.tekton.com.demo.presentation.view.listeners.MainListener;

/**
 * Created by hsmedina on 3/16/2018.
 */

public class HomeActivity extends AppCompatActivity implements DetailsListener, MainListener {

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mFragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            showMainFragment();
        }

    }


    @Override
    public void showDetailsEvent(Bundle event) {
        showDetailsEvenet(event);
    }


    @Override
    public void showMainHome() {
        showMainFragment();
    }

    public void showDetailsEvenet(Bundle event){
        EventsDetailFragment detailFragment = new EventsDetailFragment();
        detailFragment.setArguments(event);
        loadFragment(detailFragment);
    }

    public void showMainFragment(){
        MainFragment mainFragment = new MainFragment();
        loadFragment(mainFragment);
    }

    private void loadFragment(Fragment fg){
        if (fg != null){
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.container, fg);
            mFragmentTransaction.commit();
            getSupportFragmentManager().executePendingTransactions();
        }
    }


}
