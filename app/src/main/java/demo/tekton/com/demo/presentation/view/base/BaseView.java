package demo.tekton.com.demo.presentation.view.base;

import android.content.Context;

/**
 * Created by hsmedina on 3/16/2018.
 */

public interface BaseView {
    Context getContext();
    void showLoading();
    void hideLoading();
    void showMessage();
    void showErrorMessage(String errorMessage);
}
