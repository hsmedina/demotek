package demo.tekton.com.demo.presentation.view.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import demo.tekton.com.demo.R;
import demo.tekton.com.demo.di.components.DaggerListEventComponent;
import demo.tekton.com.demo.di.components.ListEventComponent;
import demo.tekton.com.demo.di.modules.ListEventModule;
import demo.tekton.com.demo.domain.model.EventModel;

import demo.tekton.com.demo.presentation.presenter.ListEventPresenterImpl;
import demo.tekton.com.demo.presentation.view.activities.RegisterEvent;
import demo.tekton.com.demo.presentation.view.adapters.EventAdapter;
import demo.tekton.com.demo.presentation.view.contracts.ListEventsContract;
import demo.tekton.com.demo.presentation.view.listeners.DetailsListener;
import demo.tekton.com.demo.presentation.view.listeners.ItemClickSupport;
import demo.tekton.com.demo.presentation.view.util.Util;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class EventsFragment extends Fragment implements ListEventsContract.view {

    private Context context;
    private AppCompatActivity activity;
    private View view;
    ListEventComponent component;
    EventAdapter adapter;
    @BindView(R.id.rv) RecyclerView rv;
    @BindView(R.id.loader) ProgressBar loader;
    @Inject ListEventPresenterImpl presenter;
    private DetailsListener listener;
    private List<EventModel> eventModels;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDagger();
        activity = (AppCompatActivity) getActivity();
        context = activity.getApplicationContext();
        presenter.setView(this);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        Activity a;

        if (context instanceof Activity){
            a=(Activity) context;
            listener = (DetailsListener) a;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        view = inflater.inflate(R.layout.fragment_events, null, false);

        ButterKnife.bind(this,view);

        ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                listener.showDetailsEvent(getEvent(position));
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getEvents();
    }

    @OnClick(R.id.new_event) void regNewEvent(){
        context.startActivity(new Intent(context, RegisterEvent.class));
    }


    @Override
    public void showLoading() {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loader.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMessage() {

    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Util.showMessager(rv,"Failed to show events");
    }

    @Override
    public void getEvents() {
        presenter.getEvents();
    }

    @Override
    public void showEvents(List<EventModel> eventModels) {
        this.eventModels=eventModels;
        adapter = new EventAdapter(context, eventModels,null);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setAdapter(adapter);

    }

    @Override
    public void emptyData() {

    }

    @Override
    public void getDetailsEvent(String id) {

    }

    @Override
    public void showDetailsEvent(EventModel eventModel) {

    }

    private void initDagger(){
        if (component == null) {
            component = DaggerListEventComponent.builder()
                    .listEventModule(new ListEventModule())
                    .build();
            component.inject(this);
        }
    }

    private Bundle getEvent(int position){
        Bundle args = new Bundle();
        args.putSerializable(getString(R.string.key), this.eventModels.get(position));
        return args;
    }


}
