package demo.tekton.com.demo.presentation.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import demo.tekton.com.demo.R;
import demo.tekton.com.demo.di.components.DaggerUserComponent;
import demo.tekton.com.demo.di.components.UserComponent;
import demo.tekton.com.demo.di.modules.UserModule;
import demo.tekton.com.demo.presentation.TekApplication;
import demo.tekton.com.demo.presentation.presenter.RegisterPresenterImpl;
import demo.tekton.com.demo.presentation.view.contracts.RegisterContract;
import demo.tekton.com.demo.presentation.view.util.Util;

/**
 * Created by hsmedina on 3/16/2018.
 */

public class RegisterActivity extends AppCompatActivity implements RegisterContract.view {

    @BindView(R.id.user) EditText user;
    @BindView(R.id.pass) EditText pass;
    @BindView(R.id.confpass) EditText confPass;
    @BindView(R.id.loader) ProgressBar loader;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @Inject  RegisterPresenterImpl presenter;
    UserComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        initDagger();
        initToolBar();

        presenter.setView(this);
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loader.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMessage() {

    }

    @Override
    public void showErrorMessage(String errorMessage) {

    }

    @Override
    public void resetView() {
        user.setText("");
        pass.setText("");
        confPass.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case android.R.id.home:
                goLogin();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void doRegister(String email, String pass) {
        presenter.doRegister(email,pass);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goLogin();
    }

    @OnClick(R.id.btnreg) void doClickRegister(){
        if(Util.validationEmpty(this,user,pass)){
            if(Util.validationPassword(this,pass, confPass)) {
                doRegister(user.getText().toString(), pass.getText().toString());
            }
        }
    }


    @Override
    public void regSuccess(boolean value) {

        if(value) {
            resetView();
            Util.showMessager(pass, getString(R.string.registerSuccess));
        }else{
            Util.showMessager(pass, getString(R.string.registerFailed));
        }
    }

    @Override
    public void goLogin() {
        resetView();
        startActivity(new Intent(this,LoginActivity.class));
    }

    private void initDagger(){
        if (component == null) {
            component = DaggerUserComponent.builder()
                    .userModule(new UserModule())
                    .build();
            component.inject(this);
        }
    }

    private void initToolBar(){

        toolbar.setBackground(Util.getDrawableImage(this, R.color.transparent));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getString(R.string.registration));

    }
}
