package demo.tekton.com.demo.domain.interactors;

import java.util.List;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.model.EventModel;
import demo.tekton.com.demo.domain.repository.ListEventRepository;
import io.reactivex.Observable;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class ListEventInteractor  extends UseCase<List<EventModel>, Void> {

    private final ListEventRepository repository;

    @Inject
    public ListEventInteractor(ListEventRepository repository){
        this.repository=repository;
    }


    @Override
    Observable<List<EventModel>> buildUseCaseObservable(Void aVoid) {
        return repository.getEvents();
    }

}