package demo.tekton.com.demo.domain.interactors;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.repository.UserRepository;
import io.reactivex.Observable;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class LogoutInteractor extends UseCase<Void, Void> {

    private final UserRepository repository;

    @Inject
    public LogoutInteractor(UserRepository repository){
        this.repository=repository;
    }

    @Override
    Observable<Void> buildUseCaseObservable(Void aVoid) {
        return repository.logoutUser();
    }

}
