package demo.tekton.com.demo.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import demo.tekton.com.demo.data.repository.EventDataRepository;
import demo.tekton.com.demo.data.repository.ListEventDataRepository;
import demo.tekton.com.demo.data.repository.datasource.EventDataSource;
import demo.tekton.com.demo.data.repository.datasource.ListEventDataSource;
import demo.tekton.com.demo.domain.repository.EventRepository;
import demo.tekton.com.demo.domain.repository.ListEventRepository;

/**
 * Created by hsmedina on 3/19/2018.
 */

@Module
public class EventModule {

    @Provides
    @Singleton
    EventDataSource provideEventDataSource() {
        return new EventDataSource();
    }

    @Provides
    @Singleton
    EventRepository provideEventRepository(EventDataRepository eventDataRepository) {
        return eventDataRepository;
    }





}
