package demo.tekton.com.demo.data.repository;

import com.google.firebase.database.DataSnapshot;

import java.util.List;

import javax.inject.Inject;

import demo.tekton.com.demo.data.entity.EventEntity;
import demo.tekton.com.demo.data.entity.mapper.EventMapper;
import demo.tekton.com.demo.data.repository.datasource.ListEventDataSource;
import demo.tekton.com.demo.domain.model.EventModel;
import demo.tekton.com.demo.domain.repository.ListEventRepository;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class ListEventDataRepository implements ListEventRepository {

    ListEventDataSource dataSource;

    @Inject
    public ListEventDataRepository(ListEventDataSource dataSource){
        this.dataSource=dataSource;
    }

    @Override
    public Observable<List<EventModel>> getEvents() {
        return this.dataSource.getEvents().map(new Function<DataSnapshot, List<EventEntity>>() {
            @Override
            public List<EventEntity> apply(DataSnapshot dataSnapshot) throws Exception {
                return EventMapper.transformDataSnapshotToEventEntities(dataSnapshot);
            }
        }).map(new Function<List<EventEntity>, List<EventModel>>() {
            @Override
            public List<EventModel> apply(List<EventEntity> eventEntities) throws Exception {
                return EventMapper.transformList(eventEntities);
            }
        });

    }

    @Override
    public Observable<EventEntity> getEvent(String eventId) {
        return null;
    }
}
