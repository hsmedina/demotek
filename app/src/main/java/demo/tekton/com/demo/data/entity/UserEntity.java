package demo.tekton.com.demo.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class UserEntity {

    private String id = null;
    private String email = null;
    private String username = null;
    private String subscription = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }
}
