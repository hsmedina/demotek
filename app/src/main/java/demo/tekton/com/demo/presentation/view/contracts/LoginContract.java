package demo.tekton.com.demo.presentation.view.contracts;

import demo.tekton.com.demo.presentation.presenter.BasePesenter;
import demo.tekton.com.demo.presentation.view.base.BaseView;

/**
 * Created by hsmedina on 3/16/2018.
 */

public interface LoginContract {

    interface view extends BaseView{
        void doLogin(String email, String pass);
        void goRegister();
        void goHome();
    }

    interface presenter extends BasePesenter<view> {
        void doLogin(String email, String pass);
        void loginSuccess();
        void loginFailed();
    }
}
