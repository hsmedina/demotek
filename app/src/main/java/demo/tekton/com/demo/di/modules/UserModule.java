package demo.tekton.com.demo.di.modules;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import demo.tekton.com.demo.data.repository.UserDataRepository;
import demo.tekton.com.demo.data.repository.datasource.UserDataSource;
import demo.tekton.com.demo.domain.repository.UserRepository;

/**
 * Created by hsmedina on 3/18/2018.
 */


@Module
public class UserModule {

    @Provides
    @Singleton
    UserDataSource provideUserDataSource() {
        return new UserDataSource();
    }

    @Provides
    @Singleton
    UserRepository provideUserRepository(UserDataRepository userDataRepository) {
        return userDataRepository;
    }


}
