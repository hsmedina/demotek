package demo.tekton.com.demo.presentation.presenter;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.interactors.DefaultObserver;
import demo.tekton.com.demo.domain.interactors.LoggedInteractor;
import demo.tekton.com.demo.domain.interactors.LogoutInteractor;
import demo.tekton.com.demo.presentation.view.contracts.LogoutContract;
import demo.tekton.com.demo.presentation.view.contracts.SplashContract;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class LogoutPresenterImpl  implements LogoutContract.presenter {


    private LogoutContract.view view;
    private final LogoutInteractor interactor;

    @Inject
    public LogoutPresenterImpl(LogoutInteractor interactor){
        this.interactor=interactor;
    }

    @Override
    public void setView(LogoutContract.view view) {
        this.view=view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.interactor.dispose();
        this.view=null;
    }

    @Override
    public void singOut() {
        this.interactor.execute(new LogoutPresenterImpl.LogoutUserObserver(), null);
    }


    @Override
    public void goLogin() {
        this.view.goLogin();
    }


    private final class LogoutUserObserver extends DefaultObserver<Void> {

        @Override
        public void onNext(Void voiD) {
            super.onNext(voiD);
            LogoutPresenterImpl.this.goLogin();
        }

        @Override public void onComplete() {

        }

        @Override public void onError(Throwable e) {
            LogoutPresenterImpl.this.goLogin();
        }

    }
}

