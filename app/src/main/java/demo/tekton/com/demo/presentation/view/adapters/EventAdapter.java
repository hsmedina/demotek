package demo.tekton.com.demo.presentation.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import demo.tekton.com.demo.R;
import demo.tekton.com.demo.domain.model.EventModel;
import demo.tekton.com.demo.presentation.view.util.Util;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class EventAdapter  extends RecyclerView.Adapter<EventAdapter.EventsHolder> {


    public Context context;
    List<EventModel> items;
    Util helper;

    public EventAdapter(Context context, List<EventModel> items, Util helper){
        this.context=context;
        this.items=items;
        this.helper=helper;
    }

    @Override
    public EventsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_events, parent, false);
        EventsHolder vh = new EventsHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(EventsHolder holder, int position) {
        EventModel eventItem = (EventModel) items.get(position);

        holder.title.setText(eventItem.getName());
        holder.date.setText(eventItem.getDate());
        holder.time.setText(eventItem.getTime());
        //holder.location.setText(eventItem.getLocation());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class EventsHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.new_event) TextView title;
        @BindView(R.id.date_event) TextView date;
        @BindView(R.id.time_event) TextView time;
        @BindView(R.id.address_event) TextView location;


        public EventsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
