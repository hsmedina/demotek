package demo.tekton.com.demo.presentation.presenter;

import android.util.Log;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.interactors.DefaultObserver;
import demo.tekton.com.demo.domain.interactors.RegisterEventInteractor;
import demo.tekton.com.demo.domain.util.Constants;
import demo.tekton.com.demo.domain.util.Params;
import demo.tekton.com.demo.presentation.view.contracts.RegisterEventContract;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class RegisterEventPresenterImpl  implements RegisterEventContract.presenter {


    private RegisterEventContract.view view;
    private final RegisterEventInteractor interactor;


    @Inject
    public RegisterEventPresenterImpl(RegisterEventInteractor interactor){
        this.interactor=interactor;
    }

    @Override
    public void setView(RegisterEventContract.view view) {
        this.view=view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.interactor.dispose();
        this.view=null;
    }


    @Override
    public void createEvent(String name, String date, String time, String location) {
        showViewLoading();
        Params params = Params.create();
        params.putString(Constants.PARAMS_EVENT_NAME, name);
        params.putString(Constants.PARAMS_EVENT_DATE, date);
        params.putString(Constants.PARAMS_EVENT_TIME, time);
        params.putString(Constants.PARAMS_EVENT_LOCATION, location);

        this.interactor.execute(new CreateEventObserver(), params);
    }

    @Override
    public void createSuccess() {
        this.view.createSuccess();
    }

    @Override
    public void createFailed() {
        this.view.createFalied();
    }


    private void showViewLoading() {
        this.view.showLoading();
    }

    private void hideViewLoading() {
        this.view.hideLoading();
    }


    private final class CreateEventObserver extends DefaultObserver<Object> {


        @Override
        public void onNext(Object object) {
            super.onNext(object);
            RegisterEventPresenterImpl.this.hideViewLoading();
            RegisterEventPresenterImpl.this.createSuccess();
        }

        @Override
        public void onComplete() {
            super.onComplete();
        }

        @Override
        public void onError(Throwable exception) {
            super.onError(exception);
            RegisterEventPresenterImpl.this.hideViewLoading();
            RegisterEventPresenterImpl.this.createFailed();
        }
    }
}
