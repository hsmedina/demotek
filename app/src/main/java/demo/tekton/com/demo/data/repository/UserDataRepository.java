package demo.tekton.com.demo.data.repository;

import android.util.Log;

import com.google.firebase.auth.AuthResult;

import javax.inject.Inject;

import demo.tekton.com.demo.data.entity.UserEntity;
import demo.tekton.com.demo.data.entity.mapper.UserMapper;
import demo.tekton.com.demo.data.repository.datasource.UserDataSource;
import demo.tekton.com.demo.domain.model.UserModel;
import demo.tekton.com.demo.domain.repository.UserRepository;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by hsmedina on 3/16/2018.
 */

public class UserDataRepository implements UserRepository {

    UserDataSource dataSource;


    @Inject
    public UserDataRepository(UserDataSource dataSource){
        this.dataSource=dataSource;
    }


    @Override
    public Observable<UserModel> registerUser(String user, String pass) {

        return this.dataSource.register(user,pass)
                .map(new Function<AuthResult, UserEntity>() {
                    @Override
                    public UserEntity apply(AuthResult authResult) throws Exception {
                        return UserMapper.transform(authResult);
                    }
                })
                .map(new Function<UserEntity, UserModel>() {
                    @Override
                    public UserModel apply(UserEntity userEntity) throws Exception {

                        return UserMapper.transform(userEntity);
                    }
                });
    }

    @Override
    public Observable<UserModel> doLogin(String user, String pass) {
        return this.dataSource.login(user,pass)
                .map(new Function<AuthResult, UserEntity>() {
                    @Override
                    public UserEntity apply(AuthResult authResult) throws Exception {
                        return UserMapper.transform(authResult);
                    }
                })
                .map(new Function<UserEntity, UserModel>() {
                    @Override
                    public UserModel apply(UserEntity userEntity) throws Exception {
                        return UserMapper.transform(userEntity);
                    }
                });
    }


    @Override
    public Observable<Boolean> isLogged() {
        return this.dataSource.isUserLogged();
    }

    @Override
    public Observable<Void> logoutUser() {
        return this.dataSource.logoutUser();
    }

    @Override
    public Observable<Object> saveUserInDB(String uid, String user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(uid);
        userEntity.setEmail(user);
        userEntity.setUsername(user);

        return this.dataSource.saveUserInDatabase(userEntity);
    }

}
