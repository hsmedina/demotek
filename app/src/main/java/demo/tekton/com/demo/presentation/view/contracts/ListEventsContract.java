package demo.tekton.com.demo.presentation.view.contracts;

import java.util.List;

import demo.tekton.com.demo.domain.model.EventModel;
import demo.tekton.com.demo.presentation.presenter.BasePesenter;
import demo.tekton.com.demo.presentation.view.base.BaseView;

/**
 * Created by hsmedina on 3/19/2018.
 */

public interface ListEventsContract {

    interface view extends BaseView {
        void getEvents();
        void showEvents(List<EventModel> eventModels);
        void emptyData();
        void getDetailsEvent(String id);
        void showDetailsEvent(EventModel eventModel);

    }

    interface presenter extends BasePesenter<ListEventsContract.view> {
        void getEvents();
        void showEvents(List<EventModel> eventModels);
        void getDetailsEvent(String id);
        void showDetailsEvent(EventModel eventModel);
        void getFailed();
    }
}
