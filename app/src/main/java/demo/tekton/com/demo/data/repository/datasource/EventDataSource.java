package demo.tekton.com.demo.data.repository.datasource;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import demo.tekton.com.demo.data.FireUtil.RxFirebase;
import demo.tekton.com.demo.data.entity.EventEntity;
import demo.tekton.com.demo.data.entity.UserEntity;
import demo.tekton.com.demo.domain.model.EventModel;
import io.reactivex.Observable;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class EventDataSource {

    private FirebaseDatabase firebaseDatabase;
    private final static String FIREBASE_CHILD_KEY_EVENTS = "events";


    public EventDataSource() {
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        //this.firebaseDatabase.setPersistenceEnabled(true);
    }

    public Observable<Object> saveEventInDatabase(EventEntity eventEntity) {

        String eventId = firebaseDatabase.getReference().push().getKey();

        DatabaseReference targetChild = this.firebaseDatabase.
                getReference()
                .child(FIREBASE_CHILD_KEY_EVENTS)
                .child(eventId);

        return RxFirebase.getObservable(targetChild.setValue(eventEntity), new RxFirebase.FirebaseTaskResponseSuccess());
    }



}
