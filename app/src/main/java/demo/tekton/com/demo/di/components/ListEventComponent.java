package demo.tekton.com.demo.di.components;

import javax.inject.Singleton;

import dagger.Component;
import demo.tekton.com.demo.di.modules.ListEventModule;
import demo.tekton.com.demo.presentation.view.fragments.EventsFragment;

/**
 * Created by hsmedina on 3/19/2018.
 */

@Singleton
@Component(modules = ListEventModule.class)
public interface ListEventComponent {
    void inject(EventsFragment view);
}
