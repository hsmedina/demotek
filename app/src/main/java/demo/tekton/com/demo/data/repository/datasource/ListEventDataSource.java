package demo.tekton.com.demo.data.repository.datasource;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;

import demo.tekton.com.demo.data.FireUtil.RxFirebase;
import demo.tekton.com.demo.data.entity.EventEntity;
import io.reactivex.Observable;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class ListEventDataSource {

    private FirebaseDatabase firebaseDatabase;
    private final static String FIREBASE_CHILD_KEY_EVENTS = "events";

    public ListEventDataSource() {
        this.firebaseDatabase = FirebaseDatabase.getInstance();
    }

    public Observable<DataSnapshot> getEvents() {
        Query query = firebaseDatabase.getReference()
                .child(FIREBASE_CHILD_KEY_EVENTS);
        return RxFirebase.getObservableDataSnapshot(query);
    }

}
