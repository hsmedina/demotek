package demo.tekton.com.demo.presentation.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import demo.tekton.com.demo.R;
import demo.tekton.com.demo.di.components.DaggerUserComponent;
import demo.tekton.com.demo.di.components.UserComponent;
import demo.tekton.com.demo.di.modules.UserModule;
import demo.tekton.com.demo.presentation.presenter.SplashPresenterImpl;
import demo.tekton.com.demo.presentation.view.contracts.SplashContract;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class SplashActivity extends AppCompatActivity implements SplashContract.view {


    @Inject SplashPresenterImpl presenter;
    UserComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initDagger();
        presenter.setView(this);
        presenter.checkSession();
    }


    @Override
    public void goLogin() {
        startActivity(new Intent(this,LoginActivity.class));
    }

    @Override
    public void goHome() {
        startActivity(new Intent(this,HomeActivity.class));
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage() {

    }

    @Override
    public void showErrorMessage(String errorMessage) {

    }

    private void initDagger(){
        if (component == null) {
            component = DaggerUserComponent.builder()
                    .userModule(new UserModule())
                    .build();
            component.inject(this);
        }
    }
}
