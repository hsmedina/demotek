package demo.tekton.com.demo.domain.util;

/**
 * Created by hsmedina on 3/16/2018.
 */

public class Constants {
    public static final String PARAM_USER="user";
    public static final String PARAM_PASS="pass";
    public final static String PARAMS_USER_ID = "id";

    public final static String PARAMS_EVENT_NAME = "event_name";
    public final static String PARAMS_EVENT_DATE = "event_date";
    public final static String PARAMS_EVENT_TIME = "event_time";
    public final static String PARAMS_EVENT_LOCATION = "event_location";
}
