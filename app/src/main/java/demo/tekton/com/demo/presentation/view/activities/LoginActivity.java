package demo.tekton.com.demo.presentation.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import demo.tekton.com.demo.R;
import demo.tekton.com.demo.di.components.DaggerUserComponent;
import demo.tekton.com.demo.di.components.UserComponent;
import demo.tekton.com.demo.di.modules.UserModule;
import demo.tekton.com.demo.presentation.TekApplication;
import demo.tekton.com.demo.presentation.presenter.LoginPresenterImpl;
import demo.tekton.com.demo.presentation.view.contracts.LoginContract;
import demo.tekton.com.demo.presentation.view.util.Util;

/**
 * Created by hsmedina on 3/16/2018.
 */

public class LoginActivity extends AppCompatActivity implements LoginContract.view {

    @BindView(R.id.user) EditText user;
    @BindView(R.id.pass) EditText pass;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.loader) ProgressBar loader;
    @Inject LoginPresenterImpl presenter;
    UserComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initDagger();
        initToolBar();

        presenter.setView(this);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loader.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMessage() {

    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Util.showMessager(pass,getString(R.string.loginFailed));
    }

    @Override
    public void doLogin(String email, String pass) {
        presenter.doLogin(email,pass);
    }

    @Override
    public void goRegister() {
        startActivity(new Intent(this,RegisterActivity.class));
    }

    @OnClick(R.id.btnlog) void login(){
        if(Util.validationEmpty(this,user,pass))
            doLogin(user.getText().toString(),pass.getText().toString());

    }

    @OnClick(R.id.btnreg) void signUp(){
        goRegister();
    }

    @Override
    public void goHome() {
        Toast.makeText(this, getString(R.string.loginSuccess), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this,HomeActivity.class));
    }

    private void initDagger(){
        if (component == null) {
            component = DaggerUserComponent.builder()
                    .userModule(new UserModule())
                    .build();
            component.inject(this);
        }
    }

    private void initToolBar(){

        toolbar.setBackground(Util.getDrawableImage(this, R.color.transparent));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

    }
}
