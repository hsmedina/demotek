package demo.tekton.com.demo.data.entity.mapper;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

import demo.tekton.com.demo.data.entity.EventEntity;
import demo.tekton.com.demo.domain.model.EventModel;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class EventMapper {

    public EventMapper(){

    }


    public static EventModel transform(EventEntity eventEntity){
        EventModel eventModel = null;
        if(eventEntity!=null){
            eventModel = new EventModel(eventEntity.getId(),eventEntity.getName(),eventEntity.getDate(),eventEntity.getTime(),eventEntity.getLocation());
        }
        return eventModel;
    }

    public static List<EventModel> transformList(List<EventEntity> eventEntities){
        List<EventModel> townships = new ArrayList<>();
        for(EventEntity eventEntity:eventEntities){
            if(eventEntity!=null){
                townships.add(transform(eventEntity));
            }
        }
        return townships;
    }

    public static List<EventEntity> transformDataSnapshotToEventEntities(DataSnapshot snapshot){
        List<EventEntity> eventEntities = new ArrayList<>();
        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
            EventEntity eventEntity = postSnapshot.getValue(EventEntity.class);
            eventEntities.add(eventEntity);
        }

        return eventEntities;
    }


}
