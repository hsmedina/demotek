package demo.tekton.com.demo.di.components;

import javax.inject.Singleton;

import dagger.Component;
import demo.tekton.com.demo.di.modules.EventModule;
import demo.tekton.com.demo.presentation.view.activities.RegisterEvent;

/**
 * Created by hsmedina on 3/19/2018.
 */
@Singleton
@Component(modules = EventModule.class)
public interface EventComponent {
    void inject(RegisterEvent view);
}
