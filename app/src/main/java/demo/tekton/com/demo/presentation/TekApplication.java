package demo.tekton.com.demo.presentation;

import android.app.Application;
import android.content.Context;

import demo.tekton.com.demo.di.components.AppComponent;
import demo.tekton.com.demo.di.components.DaggerAppComponent;
import demo.tekton.com.demo.di.modules.AppModule;
import demo.tekton.com.demo.presentation.view.util.TypefaceUtil;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class TekApplication extends Application {

    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        TypefaceUtil.overrideFont(getApplicationContext(), "DEFAULT", "Montserrat-Light.otf");
        TypefaceUtil.overrideFont(getApplicationContext(), "MONOSPACE", "Montserrat-Light.otf");
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "Montserrat-Light.otf");
        TypefaceUtil.overrideFont(getApplicationContext(), "SANS_SERIF", "Montserrat-Light.otf");

    }

    public static TekApplication get(Context context) {
        return (TekApplication) context.getApplicationContext();
    }


    public AppComponent getComponent() {
        return component;
    }
}
