package demo.tekton.com.demo.domain.repository;

import demo.tekton.com.demo.domain.model.UserModel;
import io.reactivex.Observable;

/**
 * Created by hsmedina on 3/16/2018.
 */

public interface UserRepository {
    Observable<UserModel> registerUser(String user, String pass);
    Observable<UserModel> doLogin(String user, String pass);
    Observable<Boolean> isLogged();
    Observable<Void> logoutUser();
    Observable<Object> saveUserInDB(String uid, String user);

}
