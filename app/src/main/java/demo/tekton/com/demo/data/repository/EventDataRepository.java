package demo.tekton.com.demo.data.repository;

import javax.inject.Inject;

import demo.tekton.com.demo.data.entity.EventEntity;
import demo.tekton.com.demo.data.repository.datasource.EventDataSource;
import demo.tekton.com.demo.domain.model.EventModel;
import demo.tekton.com.demo.domain.repository.EventRepository;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class EventDataRepository implements EventRepository {

    EventDataSource dataSource;


    @Inject
    public EventDataRepository(EventDataSource dataSource){
        this.dataSource=dataSource;
    }


    @Override
    public Observable<Object> registerUser(String name, String date, String time, String place) {
        EventEntity eventEntity = new EventEntity();
        eventEntity.setName(name);
        eventEntity.setDate(date);
        eventEntity.setTime(time);
        eventEntity.setLocation(place);

        return this.dataSource.saveEventInDatabase(eventEntity);
    }


}

