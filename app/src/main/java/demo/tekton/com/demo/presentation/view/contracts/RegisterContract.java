package demo.tekton.com.demo.presentation.view.contracts;

import demo.tekton.com.demo.domain.model.UserModel;
import demo.tekton.com.demo.presentation.presenter.BasePesenter;
import demo.tekton.com.demo.presentation.view.base.BaseView;

/**
 * Created by hsmedina on 3/16/2018.
 */

public interface RegisterContract {

    interface view extends BaseView {
        void doRegister(String email, String pass);
        void goLogin();
        void regSuccess(boolean value);
        void resetView();
    }

    interface presenter extends BasePesenter<RegisterContract.view> {
        void doRegister(String email, String pass);
        void saveUserInDB(String uid, String username);
        void registerSuccess(UserModel userModel);
        void registerFailed();
    }

}
