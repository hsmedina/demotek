package demo.tekton.com.demo.domain.interactors;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.model.UserModel;
import demo.tekton.com.demo.domain.repository.UserRepository;
import demo.tekton.com.demo.domain.util.Constants;
import demo.tekton.com.demo.domain.util.Params;
import io.reactivex.Observable;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class LoginUserInteractor  extends UseCase<UserModel, Params> {

    private final UserRepository repository;

    @Inject
    public LoginUserInteractor(UserRepository repository){
        this.repository=repository;
    }


    @Override
    Observable<UserModel> buildUseCaseObservable(Params params) {
        return repository.doLogin(params.getString(Constants.PARAM_USER,""), params.getString(Constants.PARAM_PASS,""));
    }

}

