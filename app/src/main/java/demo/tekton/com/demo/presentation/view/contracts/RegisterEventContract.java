package demo.tekton.com.demo.presentation.view.contracts;

import demo.tekton.com.demo.presentation.presenter.BasePesenter;
import demo.tekton.com.demo.presentation.view.base.BaseView;

/**
 * Created by hsmedina on 3/19/2018.
 */

public interface RegisterEventContract {

    interface view extends BaseView {
        void createEvent(String name, String date, String time, String location);
        void createSuccess();
        void createFalied();
    }

    interface presenter extends BasePesenter<RegisterEventContract.view> {
        void createEvent(String name, String date, String time, String location);
        void createSuccess();
        void createFailed();
    }
}
