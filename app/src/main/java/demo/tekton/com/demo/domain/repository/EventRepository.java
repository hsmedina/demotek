package demo.tekton.com.demo.domain.repository;

import java.util.List;

import demo.tekton.com.demo.data.entity.EventEntity;
import demo.tekton.com.demo.domain.model.EventModel;
import io.reactivex.Observable;

/**
 * Created by hsmedina on 3/19/2018.
 */

public interface EventRepository {
    Observable<Object> registerUser(String name, String date, String time, String place);

}
