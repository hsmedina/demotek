package demo.tekton.com.demo.domain.interactors;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.model.EventModel;
import demo.tekton.com.demo.domain.repository.EventRepository;
import demo.tekton.com.demo.domain.util.Constants;
import demo.tekton.com.demo.domain.util.Params;
import io.reactivex.Observable;



public class RegisterEventInteractor extends UseCase<Object, Params> {

    private final EventRepository repository;

    @Inject
    public RegisterEventInteractor(EventRepository repository){
        this.repository=repository;
    }

    @Override
    Observable<Object> buildUseCaseObservable(Params params) {
        return repository.registerUser(params.getString(Constants.PARAMS_EVENT_NAME,""),
                params.getString(Constants.PARAMS_EVENT_DATE,""),
                params.getString(Constants.PARAMS_EVENT_TIME,""),
                params.getString(Constants.PARAMS_EVENT_LOCATION,""));
    }


}
