package demo.tekton.com.demo.domain.interactors;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.repository.UserRepository;
import demo.tekton.com.demo.domain.util.Constants;
import io.reactivex.Observable;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class LoggedInteractor   extends UseCase<Boolean, Void> {

    private final UserRepository repository;

    @Inject
    public LoggedInteractor(UserRepository repository){
        this.repository=repository;
    }


    @Override
    Observable<Boolean> buildUseCaseObservable(Void aVoid) {
        return repository.isLogged();
    }

}
