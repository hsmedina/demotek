package demo.tekton.com.demo.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import demo.tekton.com.demo.data.repository.ListEventDataRepository;
import demo.tekton.com.demo.data.repository.datasource.ListEventDataSource;
import demo.tekton.com.demo.domain.repository.ListEventRepository;

/**
 * Created by hsmedina on 3/19/2018.
 */


@Module
public class ListEventModule {

    @Provides
    @Singleton
    ListEventDataSource provideListEventDataSource() {
        return new ListEventDataSource();
    }

    @Provides
    @Singleton
    ListEventRepository provideListEventRepository(ListEventDataRepository listEventDataRepository) {
        return listEventDataRepository;
    }
}
