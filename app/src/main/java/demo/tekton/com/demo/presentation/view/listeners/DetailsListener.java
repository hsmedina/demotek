package demo.tekton.com.demo.presentation.view.listeners;

import android.os.Bundle;

/**
 * Created by hsmedina on 3/20/2018.
 */

public interface DetailsListener {
    void showDetailsEvent(Bundle event);
}
