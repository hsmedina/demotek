package demo.tekton.com.demo.presentation.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import demo.tekton.com.demo.R;
import demo.tekton.com.demo.presentation.view.activities.RegisterEvent;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class SubscribedFragment extends Fragment {

    private Context context;
    private AppCompatActivity activity;
    private View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppCompatActivity) getActivity();
        context = activity.getApplicationContext();
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        view = inflater.inflate(R.layout.fragment_subscription, null, false);

        ButterKnife.bind(this,view);

        return view;
    }

    @OnClick(R.id.new_event) void regNewEvent(){
        context.startActivity(new Intent(context, RegisterEvent.class));
    }

}
