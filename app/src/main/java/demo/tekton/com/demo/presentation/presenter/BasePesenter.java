package demo.tekton.com.demo.presentation.presenter;


import demo.tekton.com.demo.presentation.view.base.BaseView;

/**
 * Created by hsmedina on 3/13/2018.
 */

public interface BasePesenter<V extends BaseView> {
    void setView(V view);
    void resume();
    void pause();
    void destroy();
}
