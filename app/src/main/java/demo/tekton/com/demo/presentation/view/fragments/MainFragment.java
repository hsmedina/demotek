package demo.tekton.com.demo.presentation.view.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import demo.tekton.com.demo.R;
import demo.tekton.com.demo.presentation.model.TabPagerItem;
import demo.tekton.com.demo.presentation.view.activities.LogoutActivity;
import demo.tekton.com.demo.presentation.view.activities.RegisterEvent;
import demo.tekton.com.demo.presentation.view.adapters.ViewPagerAdapter;
import demo.tekton.com.demo.presentation.view.util.Util;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class MainFragment extends Fragment {

    private Context context;
    private AppCompatActivity activity;
    private View view;
    private List<TabPagerItem> mTabs = new ArrayList<>();

    @BindView(R.id.tablayout) TabLayout tabLayout;
    @BindView(R.id.viewpager) ViewPager mViewPager;
    @BindView(R.id.toolbar) Toolbar toolbar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createTabPagerItem();
        activity = (AppCompatActivity) getActivity();
        context = activity.getApplicationContext();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        view = inflater.inflate(R.layout.fragment_main, null, false);

        ButterKnife.bind(this,view);



        return view;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        activity.getMenuInflater().inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.logout:
                showDialod();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initToolBar();

        mViewPager.setOffscreenPageLimit(mTabs.size());
        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), mTabs));


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // mSlidingTabLayout.setElevation(15);
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch(position){
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();
    }

    private void createTabPagerItem(){
        mTabs.add(new TabPagerItem("", new EventsFragment()));
        mTabs.add(new TabPagerItem("", new SubscribedFragment()));
    }

    private void setupTabIcons() {

       // ImageView tabOne = (ImageView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
       // tabOne.setImageDrawable(Util.getDrawableImage(context, R.drawable.ic_event));

        TextView tabOne = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabOne.setText(getString(R.string.tab1));
        tabOne.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_event , 0 , 0, 0);


        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabTwo.setText(getString(R.string.tab2));
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_susbcriptions , 0 , 0, 0);

        //ImageView tabTwo = (ImageView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        //tabTwo.setImageDrawable(Util.getDrawableImage(context, R.drawable.ic_susbcriptions));
        tabLayout.getTabAt(1).setCustomView(tabTwo);

    }

    private void initToolBar(){

        //toolbar.setTitle(getString(R.string.home_title));
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(getString(R.string.home_title));


    }

    private void showDialod(){

            new AlertDialog.Builder(activity)
                    .setTitle(getString(R.string.app_name))
                    .setMessage(getString(R.string.desc_logout))
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            context.startActivity(new Intent(context, LogoutActivity.class));
                        }
                    }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).show();

    }



}
