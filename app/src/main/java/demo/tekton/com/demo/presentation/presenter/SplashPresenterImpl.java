package demo.tekton.com.demo.presentation.presenter;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.interactors.DefaultObserver;
import demo.tekton.com.demo.domain.interactors.LoggedInteractor;
import demo.tekton.com.demo.presentation.view.contracts.SplashContract;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class SplashPresenterImpl   implements SplashContract.presenter {


    private SplashContract.view view;
    private final LoggedInteractor interactor;

    @Inject
    public SplashPresenterImpl(LoggedInteractor interactor){
        this.interactor=interactor;
    }

    @Override
    public void setView(SplashContract.view view) {
        this.view=view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.interactor.dispose();
        this.view=null;
    }


    @Override
    public void checkSession() {
        this.interactor.execute(new SplashPresenterImpl.LoggedUserObserver(), null);
    }

    @Override
    public void goLogin() {
        this.view.goLogin();
    }

    @Override
    public void goHome() {
        this.view.goHome();
    }

    private final class LoggedUserObserver extends DefaultObserver<Boolean> {

        @Override
        public void onNext(Boolean isLogged) {
            super.onNext(isLogged);

            if(isLogged)
                SplashPresenterImpl.this.goHome();
            else
                SplashPresenterImpl.this.goLogin();
        }

        @Override public void onComplete() {

        }

        @Override public void onError(Throwable e) {
            SplashPresenterImpl.this.goLogin();
        }

    }
}

