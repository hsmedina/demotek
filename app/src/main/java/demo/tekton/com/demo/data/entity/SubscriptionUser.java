package demo.tekton.com.demo.data.entity;

/**
 * Created by hsmedina on 3/20/2018.
 */

public class SubscriptionUser {

    String event_suscription;

    public SubscriptionUser() {
    }

    public SubscriptionUser(String event_suscription) {
        this.event_suscription = event_suscription;
    }

    public String getEvent_suscription() {
        return event_suscription;
    }

    public void setEvent_suscription(String event_suscription) {
        this.event_suscription = event_suscription;
    }
}
