package demo.tekton.com.demo.presentation.view.contracts;

import demo.tekton.com.demo.presentation.presenter.BasePesenter;
import demo.tekton.com.demo.presentation.view.base.BaseView;

/**
 * Created by hsmedina on 3/19/2018.
 */

public interface LogoutContract {
    interface view extends BaseView {
        void goLogin();
    }

    interface presenter extends BasePesenter<LogoutContract.view> {
        void singOut();
        void goLogin();

    }
}
