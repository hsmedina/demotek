package demo.tekton.com.demo.presentation.presenter;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.interactors.DefaultObserver;
import demo.tekton.com.demo.domain.interactors.LoginUserInteractor;
import demo.tekton.com.demo.domain.interactors.RegisterUserInteractor;
import demo.tekton.com.demo.domain.model.UserModel;
import demo.tekton.com.demo.domain.util.Constants;
import demo.tekton.com.demo.domain.util.Params;
import demo.tekton.com.demo.presentation.view.contracts.LoginContract;
import demo.tekton.com.demo.presentation.view.contracts.RegisterContract;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class LoginPresenterImpl  implements LoginContract.presenter {


    private LoginContract.view view;
    private final LoginUserInteractor interactor;

    @Inject
    public LoginPresenterImpl(LoginUserInteractor interactor){
        this.interactor=interactor;
    }

    @Override
    public void setView(LoginContract.view view) {
        this.view=view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.interactor.dispose();
        this.view=null;
    }


    @Override
    public void doLogin(String email, String pass) {
        showViewLoading();
        Params params = Params.create();
        params.putString(Constants.PARAM_USER, email);
        params.putString(Constants.PARAM_PASS, pass);

        this.interactor.execute(new LoginPresenterImpl.LoginUserObserver(), params);
    }

    @Override
    public void loginSuccess() {
        this.view.goHome();
    }

    @Override
    public void loginFailed() {
        this.view.showErrorMessage("");
    }

    private void showViewLoading() {
        this.view.showLoading();
    }

    private void hideViewLoading() {
        this.view.hideLoading();
    }

    private final class LoginUserObserver extends DefaultObserver<UserModel> {

        @Override
        public void onNext(UserModel userModel) {
            super.onNext(userModel);
            LoginPresenterImpl.this.hideViewLoading();
            LoginPresenterImpl.this.loginSuccess();
        }

        @Override public void onComplete() {

        }

        @Override public void onError(Throwable e) {
            LoginPresenterImpl.this.hideViewLoading();
            LoginPresenterImpl.this.loginFailed();
        }

    }
}

