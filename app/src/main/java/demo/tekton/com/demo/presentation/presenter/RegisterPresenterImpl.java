package demo.tekton.com.demo.presentation.presenter;

import android.util.Log;

import javax.inject.Inject;

import demo.tekton.com.demo.data.FireUtil.RxFirebase;
import demo.tekton.com.demo.domain.interactors.SaveUserInteractor;
import demo.tekton.com.demo.domain.model.UserModel;
import demo.tekton.com.demo.domain.util.Constants;
import demo.tekton.com.demo.domain.util.Params;
import demo.tekton.com.demo.domain.interactors.DefaultObserver;
import demo.tekton.com.demo.domain.interactors.RegisterUserInteractor;
import demo.tekton.com.demo.presentation.view.contracts.LoginContract;
import demo.tekton.com.demo.presentation.view.contracts.RegisterContract;

/**
 * Created by hsmedina on 3/16/2018.
 */

public class RegisterPresenterImpl implements RegisterContract.presenter {


    private RegisterContract.view view;
    private final RegisterUserInteractor registerUserInteractor;
    private final SaveUserInteractor saverUserInteractor;

    @Inject
    public RegisterPresenterImpl(RegisterUserInteractor registerUserInteractor, SaveUserInteractor saverUserInteractor){
        this.registerUserInteractor=registerUserInteractor;
        this.saverUserInteractor=saverUserInteractor;
    }

    @Override
    public void setView(RegisterContract.view view) {
        this.view=view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.registerUserInteractor.dispose();
        this.saverUserInteractor.dispose();
        this.view=null;
    }


    @Override
    public void doRegister(String email, String pass) {
        showViewLoading();
        Params params = Params.create();
        params.putString(Constants.PARAM_USER, email);
        params.putString(Constants.PARAM_PASS, pass);
        this.registerUserInteractor.execute(new RegisterUserObserver(), params);
    }

    @Override
    public void registerSuccess(UserModel userModel) {

        Log.i("uid", userModel.getUid());
        Log.i("username", userModel.getEmail() +  "  user name");

        saveUserInDB(userModel.getUid(), userModel.getUsername());
    }

    @Override
    public void saveUserInDB(String uid, String username) {
        Params params = Params.create();
        params.putString(Constants.PARAMS_USER_ID, uid);
        params.putString(Constants.PARAM_USER, username);

        Log.i("uid", uid);
        Log.i("username", username +  "  user name");

        saverUserInteractor.execute(new SaveUserObserver(), params);
    }



    public  void saveUserSuccess(){
        this.view.regSuccess(true);
    }

    @Override
    public void registerFailed() {
        this.view.regSuccess(false);
    }

    private void showViewLoading() {
        this.view.showLoading();
    }

    private void hideViewLoading() {
        this.view.hideLoading();
    }

    private final class RegisterUserObserver extends DefaultObserver<UserModel> {

        @Override
        public void onNext(UserModel userModel) {
            super.onNext(userModel);
            RegisterPresenterImpl.this.hideViewLoading();
            RegisterPresenterImpl.this.registerSuccess(userModel);
        }

        @Override public void onComplete() {

        }

        @Override public void onError(Throwable e) {
            Log.i("reg error", e.getLocalizedMessage());
            Log.i("reg error", e.getMessage());
            RegisterPresenterImpl.this.hideViewLoading();
            RegisterPresenterImpl.this.registerFailed();
        }

    }

    private final class SaveUserObserver extends DefaultObserver<Object> {


        @Override
        public void onNext(Object o) {
            super.onNext(o);
            RegisterPresenterImpl.this.saveUserSuccess();
        }


        @Override
        public void onComplete() {
            super.onComplete();
        }

        @Override
        public void onError(Throwable exception) {
            super.onError(exception);
            Log.i("reg error", exception.getLocalizedMessage());
            Log.i("reg error", exception.getMessage());
            RegisterPresenterImpl.this.hideViewLoading();
            RegisterPresenterImpl.this.registerFailed();
        }
    }
}
