package demo.tekton.com.demo.data.entity.mapper;

import android.util.Log;

import com.google.firebase.auth.AuthResult;

import demo.tekton.com.demo.data.entity.UserEntity;
import demo.tekton.com.demo.domain.model.UserModel;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class UserMapper {

    public static UserModel transform(UserEntity userEntity) {
        UserModel userModel = null;
        if (userEntity != null) {
            userModel = new UserModel(userEntity.getUsername());
            userModel.setUid(userEntity.getId());
            userModel.setEmail(userEntity.getEmail());
        }

        return userModel;
    }

    public static UserEntity transform(AuthResult authResult) {
        UserEntity userEntity = null;
        if (authResult != null) {

            Log.i("AuthResult", authResult.getUser().getUid() );
            Log.i("AuthResult", authResult.getUser().getEmail() );

            userEntity = new UserEntity();
            userEntity.setId(authResult.getUser().getUid());
            userEntity.setUsername(authResult.getUser().getEmail());
            userEntity.setEmail(authResult.getUser().getEmail());
        }

        return userEntity;
    }
}
