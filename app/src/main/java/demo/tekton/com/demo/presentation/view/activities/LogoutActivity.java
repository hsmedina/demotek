package demo.tekton.com.demo.presentation.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import demo.tekton.com.demo.R;
import demo.tekton.com.demo.di.components.DaggerUserComponent;
import demo.tekton.com.demo.di.components.UserComponent;
import demo.tekton.com.demo.di.modules.UserModule;
import demo.tekton.com.demo.presentation.presenter.LogoutPresenterImpl;
import demo.tekton.com.demo.presentation.view.contracts.LogoutContract;

/**
 * Created by hsmedina on 3/20/2018.
 */

public class LogoutActivity  extends AppCompatActivity implements LogoutContract.view {



    @Inject LogoutPresenterImpl presenter;
    UserComponent component;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        initDagger();
        presenter.setView(this);

         new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                presenter.singOut();
            }
        }, 4000);

    }


    @Override
    public void goLogin() {
        startActivity(new Intent(this,LoginActivity.class));
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage() {

    }

    @Override
    public void showErrorMessage(String errorMessage) {

    }

    private void initDagger(){
        if (component == null) {
            component = DaggerUserComponent.builder()
                    .userModule(new UserModule())
                    .build();
            component.inject(this);
        }
    }
}

