package demo.tekton.com.demo.presentation.view.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import demo.tekton.com.demo.R;
import demo.tekton.com.demo.domain.model.EventModel;
import demo.tekton.com.demo.presentation.view.listeners.DetailsListener;
import demo.tekton.com.demo.presentation.view.listeners.MainListener;


/**
 * Created by hsmedina on 3/20/2018.
 */

public class EventsDetailFragment  extends Fragment {

    private Context context;
    private AppCompatActivity activity;
    private View view;

    @BindView(R.id.new_event) TextView name_event;
    @BindView(R.id.date_event) TextView date_event;
    @BindView(R.id.time_event) TextView time_event;
    @BindView(R.id.address_event) TextView address_event;
    @BindView(R.id.toolbar) Toolbar toolbar;
    private MainListener listener;
    private EventModel event=null;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (AppCompatActivity) getActivity();
        context = activity.getApplicationContext();

        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(getString(R.string.key)))
            event = (EventModel) arguments.getSerializable(getString(R.string.key));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        Activity a;

        if (context instanceof Activity){
            a=(Activity) context;
            listener = (MainListener) a;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        view = inflater.inflate(R.layout.fragment_details_events, null, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initToolBar();

        if(event!=null)
            loadData(event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case android.R.id.home:
                listener.showMainHome();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initToolBar(){

        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle("");
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);


    }

    private void loadData(EventModel eventModel){
        name_event.setText(eventModel.getName());
        date_event.setText(eventModel.getDate());
        time_event.setText(eventModel.getTime());
        address_event.setText(eventModel.getLocation());
    }



}

