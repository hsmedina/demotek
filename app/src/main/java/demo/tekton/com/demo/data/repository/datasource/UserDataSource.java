package demo.tekton.com.demo.data.repository.datasource;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import demo.tekton.com.demo.data.FireUtil.RxFirebase;
import demo.tekton.com.demo.data.entity.UserEntity;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;

/**
 * Created by hsmedina on 3/18/2018.
 */

public class UserDataSource {


    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private final static String FIREBASE_CHILD_KEY_USERS = "users";


    public UserDataSource() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
    }


    public Observable<AuthResult> register(String user, String pass) {
        return RxFirebase.getObservable(firebaseAuth.createUserWithEmailAndPassword(user, pass));
    }

    public Observable<AuthResult> login(String user, String pass) {
        return RxFirebase.getObservable(firebaseAuth.signInWithEmailAndPassword(user, pass));
    }


    public Observable<Boolean> isUserLogged() {
        return Observable.defer(new Callable<ObservableSource<? extends Boolean>>() {
            @Override
            public ObservableSource<? extends Boolean> call() throws Exception {
                return Observable.just(firebaseAuth.getCurrentUser()!=null);
            }
        });
    }

    public Observable<Void> logoutUser() {
        return Observable.defer(new Callable<ObservableSource<? extends Void>>() {
            @Override
            public ObservableSource<? extends Void> call() throws Exception {
                firebaseAuth.signOut();
                return Observable.just(null);
            }
        });
    }

    public Observable<Object> saveUserInDatabase(UserEntity userEntity) {

        DatabaseReference targetChild = firebaseDatabase.
                getReference()
                .child(FIREBASE_CHILD_KEY_USERS)
                .child(userEntity.getId());

        return RxFirebase.getObservable(targetChild.setValue(userEntity), new RxFirebase.FirebaseTaskResponseSuccess());
    }
}
