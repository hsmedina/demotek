package demo.tekton.com.demo.presentation.presenter;

import java.util.List;

import javax.inject.Inject;

import demo.tekton.com.demo.domain.interactors.DefaultObserver;
import demo.tekton.com.demo.domain.interactors.ListEventInteractor;
import demo.tekton.com.demo.domain.model.EventModel;
import demo.tekton.com.demo.presentation.view.contracts.ListEventsContract;

/**
 * Created by hsmedina on 3/19/2018.
 */

public class ListEventPresenterImpl implements ListEventsContract.presenter {


    private ListEventsContract.view view;
    private final ListEventInteractor interactor;

    @Inject
    public ListEventPresenterImpl(ListEventInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void setView(ListEventsContract.view view) {
        this.view=view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        this.interactor.dispose();
        this.view=null;
    }

    @Override
    public void getEvents() {
        showViewLoading();
        interactor.execute(new ListEventObserver(), null);
    }

    @Override
    public void showEvents(List<EventModel> eventModels) {
        view.showEvents(eventModels);
    }

    @Override
    public void getDetailsEvent(String id) {

    }

    @Override
    public void showDetailsEvent(EventModel eventModel) {

    }

    @Override
    public void getFailed() {

    }


    private void showViewLoading() {
        this.view.showLoading();
    }

    private void hideViewLoading() {
        this.view.hideLoading();
    }

    private final class ListEventObserver extends DefaultObserver<List<EventModel>> {

        @Override
        public void onNext(List<EventModel> eventModels) {
            super.onNext(eventModels);
            ListEventPresenterImpl.this.hideViewLoading();
            ListEventPresenterImpl.this.showEvents(eventModels);
        }


        @Override public void onComplete() {

        }

        @Override public void onError(Throwable e) {
            ListEventPresenterImpl.this.hideViewLoading();
            ListEventPresenterImpl.this.getFailed();
        }

    }



}
